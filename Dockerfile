FROM python:3.6

COPY requirements.txt /requirements.txt
RUN pip install -r    /requirements.txt

ADD app /app
WORKDIR /app

RUN useradd blair && chown -R blair /app
USER blair
CMD [ "python3", "sms-alerts.py" ]
