#!/usr/bin/env python3

# exports: get_billing, get_droplets
# todo: kubernetes

from typing import List, Dict
from os import getenv
import json
import requests

API_URL = 'https://api.digitalocean.com/v2/'
HEADERS = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer {0}'.format(getenv('DIGITAL_OCEAN_ACCESS_TOKEN'))
}

assert getenv('DIGITAL_OCEAN_ACCESS_TOKEN')

def api_get(endpoint):
    """ Do the api call """
    route = API_URL + endpoint
    response = requests.get(route, headers=HEADERS, timeout=123)

    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None


def get_billing() -> Dict:
    return api_get("customers/my/balance")


def get_volumes() -> List[Dict[str,str]]:
    vols = api_get("volumes")

    if vols is None:
        return None

    return [
        { k : v[k] for k in ("name", "size_gigabytes") }  
        for v in vols['volumes']
    ]


def get_k8s() -> List[Dict[str,str]]:
    ks = api_get('kubernetes/clusters')
    if ks is None:
        return None

    return [
        { 'name' : k['name'], 'node_pools' : [ 
                { kk : p[kk] for kk in ('name', 'size', 'count') }
                for p in k['node_pools']
            ]
        } for k in ks['kubernetes_clusters']
    ]
    

def get_droplets() -> List[Dict[str,str]]:
    r = api_get("droplets")
    if r is None:
        return None

    return [
        dict(
            [
                (k,d[k]) for k in
                ('name', 'status', 'size_slug')
            ] + [
                ('price_hourly', d['size']['price_hourly'] )
            ]
        )
        for d in r['droplets']
    ]
        
