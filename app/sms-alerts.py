#!/usr/bin/env python3

from blairdo import *
from os import getenv
import requests

SMS_API = getenv('SMS_API')

def report():
    k8s = get_k8s()
    droplets = get_droplets()
    volumes = get_volumes()
    billing = get_billing()

    s = '''balance: ${month_to_date_balance}'''.format(**billing)

    if k8s:
        s = s + '\nkubernetes: (w load balancer)\n'
        for k in k8s:
            for p in k['node_pools']:
                s = s + k['name'] + ': {name} - {size} - {count}\n'.format(**p)

    if droplets:
        s = s + '\ndroplets:\n'
        for droplet in droplets:
            s = s + '  {name} - {size_slug} - {status}\n'.format(**droplet)

    if volumes:
        s = s + '\nvolumes:\n'
        for volume in volumes:
            s = s + '  {name} - {size_gigabytes}\n'.format(**volume)

    print(s)

    requests.post(
        SMS_API, 
        json={'sender' : 'Digital Ocean Account', 'message' : s}
    )

if __name__ == '__main__':
    report()
